<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.05.2018
 * Time: 11:11
 * В по условиям задачи не сказано, как именно задаётся последовательность чисел по-этому создадим абсктактный класс
 * потомки которого смогут читать последовательность из различных источников и в различных форматах.
 * Для простоты реализована только возможность читать последовательность из массива
 * @see \Reader\ArrayReader
 */

namespace Reader;


abstract class AbstractReader
{
    protected $source;

    abstract public function get() :array;
}
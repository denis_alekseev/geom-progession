<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.05.2018
 * Time: 11:13
 */

namespace Reader;

/**
 * Class ArrayReader read sequence from array
 * @package Reader
 */
class ArrayReader extends AbstractReader
{
    /**
     * Проверяем параметр на то что это массив и на то, что этот массив не пустой если эти условия не выполненны кидаем исключение
     * ArrayReader constructor.
     * @param $sequence
     * @throws \Exception
     */
    public function __construct(array $sequence)
    {
        if (!is_array($sequence)) {
            throw new \Exception("Argument should be array");
        }

        if (count($sequence) == 0) {
            throw new \Exception("Array is empty");
        }

        $this->source = $sequence;
    }

    /**
     * @return mixed not empty array of elements
     * @throws \Exception
     */

    public function get() :array
    {
        return $this->source;
    }
}
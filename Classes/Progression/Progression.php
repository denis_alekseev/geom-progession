<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.05.2018
 * Time: 11:38
 */

namespace Progression;

use Reader;

class Progression
{

    public $b1;
    public $q;

    private $sequence;

    public function __construct(Reader\AbstractReader $reader)
    {
        $this->sequence = $reader->get();
        $num = count($this->sequence);
        if ($num < 3) {
            throw new \Exception("To short sequence");
        }
        $this->checkElement($this->sequence[0]);
        $this->checkElement($this->sequence[$num-1]);


        for ($i = 1; $i < $num - 1; $i++) {
            $this->checkElement($this->sequence[$i]);
            $this->checkSequenceElement($i);
        }
        $this->calculateParams();
    }

    /**
     * Проверка элемена последовательности на соответствие условиям
     * 1 число или может быть преобразован к числовому типу
     * 2 не 0 элемент если в последовательности встречаются элементы равные 0 она не может считаться геометрической прогрессией
     * @param $item
     * @throws \Exception
     */
    private function checkElement(&$item)
    {
        if (!is_numeric($item)) {
            throw new \Exception("Sequence items should be numbers");
        }

        if ($item === 0) {
            throw new \Exception("Sequence should not contain zero elements");
        }

        $item = floatval($item);
    }

    /**
     * проверка харрактеристического свойства геометрической прогрессии для каждого элемента
     * b1^2 = b(n-1)*b(n+1)
     * @param int $n
     * @throws \Exception
     */
    private function checkSequenceElement(int $n)
    {
        if ($this->sequence[$n] * $this->sequence[$n] != $this->sequence[$n - 1] * $this->sequence[$n + 1]) {
            throw new \Exception("The sequence not geometric progression wrong element " . $this->sequence[$n]);
        }
    }

    /**
     * Вычисление первого элемента и знаменателя прогрессии
     */
    private function calculateParams()
    {
        $this->b1 = $this->sequence[0];
        $this->q = $this->sequence[1]/$this->sequence[0];
    }

    public function getProgressionInfo() :string
    {
        return "Первый элемент прогрессии " . $this->b1 . "\n" .
            "Знаменатель прогрессии " . $this->q . "\n";
    }
}
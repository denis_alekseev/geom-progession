<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.05.2018
 * Time: 11:07
 */
require "./Autoloader.php";

try {
    $seq = new Progression\Progression(new Reader\ArrayReader([1, 2, 4, 8, 16, 32]));
    echo $seq->getProgressionInfo();
} catch (Exception $e) {
    echo $e->getMessage() ."\n";
}
try {
    $seq = new Progression\Progression(new Reader\ArrayReader([1, 3, 4, 8, 16, 32]));
    echo $seq->getProgressionInfo();
} catch (Exception $e) {
    echo $e->getMessage() ."\n";
}
try {
    $seq = new Progression\Progression(new Reader\ArrayReader([1, -2, 4, -8, 16, -32]));
    echo $seq->getProgressionInfo();
} catch (Exception $e) {
    echo $e->getMessage() . "\n";
}

try {
    $seq = new Progression\Progression(new Reader\ArrayReader([1, "kva", 4, -8, 16, -32]));
    echo $seq->getProgressionInfo();
} catch (Exception $e) {
    echo $e->getMessage() . "\n";
}

try {
    $seq = new Progression\Progression(new Reader\ArrayReader([1, -32]));
    echo $seq->getProgressionInfo();
} catch (Exception $e) {
    echo $e->getMessage() . "\n";
}
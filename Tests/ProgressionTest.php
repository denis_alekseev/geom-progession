<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.05.2018
 * Time: 17:12
 */

namespace Tests;
require_once '../Classes/Reader/AbstractReader.php';
require_once '../Classes/Reader/ArrayReader.php';
require_once  '../Classes/Progression/Progression.php';

use Reader\ArrayReader;
use PHPUnit\Framework\TestCase;
use Progression\Progression;

class ProgressionTest extends TestCase
{
    public function testCheckSequence() :void
    {
        $seq = new Progression(new ArrayReader([1,2,4,8,16,32]));
        $this->assertEquals(2,$seq->q);
        $this->assertEquals(1,$seq->b1);
    }

    public function testCheckNotGeometryProgression() :void
    {
        try {
            $seq = new Progression(new ArrayReader([1,3,4,8,16,32]));
        } catch(\Exception $e) {
            $this->assertEquals('The sequence not geometric progression wrong element 3', $e->getMessage());
        }
    }

    public function testCheckSequencesWithWrongParameters() :void
    {
        try {
            $seq = new Progression(new ArrayReader([1,"kva",4,8,16,32]));
        } catch(\Exception $e) {
            $this->assertEquals('Sequence items should be numbers', $e->getMessage());
        }

        try {
            $seq = new Progression(new ArrayReader([1,0,4,8,16,32]));
        } catch(\Exception $e) {
            $this->assertEquals('Sequence should not contain zero elements', $e->getMessage());
        }

        try {
            $seq = new Progression(new ArrayReader([1,32]));
        } catch(\Exception $e) {
            $this->assertEquals('To short sequence', $e->getMessage());
        }
    }
}

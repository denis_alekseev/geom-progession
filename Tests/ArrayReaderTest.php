<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.05.2018
 * Time: 16:28
 */

namespace Tests;
require_once '../Classes/Reader/AbstractReader.php';
require_once '../Classes/Reader/ArrayReader.php';

use Reader\ArrayReader;
use PHPUnit\Framework\TestCase;

class ArrayReaderTest extends TestCase
{
    public function testReadFromArray() :void
    {
        $testArray = [1,2,4,5];
        $seq = new ArrayReader($testArray);
        $this->assertEquals($testArray, $seq->get());
    }

    public function testEmptyArray() :void
    {
        $testArray = [];
        try {
            $seq = new ArrayReader($testArray);
        } catch(\Exception $e) {
            $this->assertEquals("Array is empty", $e->getMessage());
        }
    }
}
